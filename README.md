External sorting
=======================

Scope
------

This golang program implements the external sorting algorithm. The idea is that we want to sort a very big dataset (eg 200GB) efficiently.

Implementation
----------------
External sorting offloads the sorting process outside of memory, since our dataset is too big too fit in memory.
In our case:

* We split the dataset into chunks of ``12500000`` integers
* We sort each chunk using golang sort implementation
* We store it to a file
* Using a heap data structure we implement k-way-merging of the pre-sorted files
* We pop the top N numbers

Complexity
------------
* Algorithmic complexity
 * We are reading all input to split in chunks O(n)
 * For each chunk we are using golang sorting which is O(n(logn))
 * To get the top N numbers we are using a heap and k chunks
   * We are reading at least N numbers 0(n)
   * To build the base heap we need 0(k) reads and for each O(log(k)) heap pushes
   * For each element pop we need O(log(k))

In total the complexity is: o(nlog(n) for the sorting and O(nlog(k)) for the merging

* Space complexity
  * We are offloading the data outside RAM
  * In our case this is the filesystem
  * Given a dataset size N we need 2*N of storage

Concurrency
-------------
We are using concurrency to improve the performance of our program.
A reader function reads input from file (line by line) and sends the data to our data channel.
There a pull of sorting workers get the input, parses, sorts and sends the data to a writer channel.
A pull of writers gets the input and writes it to files.

Assumptions
-------------

* We assume that there is an output folder for our intermediate files.
* We assume we only deal with integers

Usage
------

In order to start a test run

```
> rm -rf data/output/**
> mkdir -p data/output
> go build sort.go
> time ./sort <path/to/file> <N>
> rm -rf data/output/**
```

Configuration
---------------

Users can configure some parameters of the program using env vars

* `SORT_PARTITION_SIZE`: chunk size (default: 12500000)
* `SORT_CONCURRENCY`: number of sorting workers (default: 100)
* `WRITE_CONCURRENCY`: number of writing workers (default: 100) 

Benchmarks
------------
Here are some benchmarks using 4core/16G RAM machine

```
> time ./sort data/1GB.txt 100
189.84s user 4.61s system 290% cpu 1:06.97 total

> time ./sort data/10GB.txt 100
1654.47s user 23.89s system 304% cpu 9:11.92 total
```

It looks like with real data time complexity scales linearly with size.

Improvements
--------------

* Cleanup intermediate files
* Implement dataset generator
* Implement unittests
* Add Makefile to make common development tasks easier
* Add a more sophisticated way to create tmp files
* Add some heuristics to fine tune worker pools and chunk size
