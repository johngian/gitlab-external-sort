package main

import (
	"bufio"
	"container/heap"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

// Error checking helper
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Helper to get env vars with default value
func getEnv(key, fallback string) int {
	value, exists := os.LookupEnv(key)
	if !exists {
		value = fallback
	}
	result, err := strconv.Atoi(value)
	check(err)
	return result
}

// Item represents the heap elements
type Item struct {
	value  int
	reader *bufio.Scanner
}

// IntHeap array
type IntHeap []*Item

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i].value > h[j].value }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

// Push implementation
func (h *IntHeap) Push(x interface{}) {
	item := x.(*Item)
	*h = append(*h, item)
}

// Pop implementation
func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

// Get top N numbers in data set
func topN(N int) {
	// Initialize heap
	h := &IntHeap{}
	heap.Init(h)

	// Iterate over sorted data
	root := "data/output/"
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		fmt.Printf("Reading file: %s\n", path)
		f, err := os.Open(path)
		check(err)

		scanner := bufio.NewScanner(f)
		scanner.Split(bufio.ScanWords)
		if scanner.Scan() {
			curr := scanner.Text()
			num, err := strconv.Atoi(curr)
			check(err)
			heap.Push(h, &Item{value: num, reader: scanner})
		}
		return nil
	})
	check(err)

	var result []int
	for i := 0; i < N; i++ {
		item := heap.Pop(h).(*Item)
		result = append(result, item.value)
		fmt.Printf("Get top #%d \n", i)
		if item.reader.Scan() {
			curr := item.reader.Text()
			num, err := strconv.Atoi(curr)
			check(err)
			heap.Push(h, &Item{value: num, reader: item.reader})
		}
	}
	fmt.Println(result)
}

// Sort input in descending order
func sortRun(lines chan string, writes chan []int, complete chan bool) {
	var buff []int
	fmt.Println("Starting sorting goroutine...")
	for line := range lines {
		words := strings.Fields(line)
		for _, word := range words {
			num, _ := strconv.Atoi(word)
			buff = append(buff, num)
		}
		if len(buff) >= getEnv("SORT_PARTITION_SIZE", "12500000") {
			sort.Sort(sort.Reverse(sort.IntSlice(buff)))
			writes <- buff
			buff = nil
		}
	}

	// Write remainders
	sort.Sort(sort.Reverse(sort.IntSlice(buff)))
	writes <- buff

	complete <- true
}

// Write integer arrays to files
func writeRun(writes chan []int, complete chan bool) {
	fmt.Println("Starting writer goroutine...")
	for buff := range writes {
		runID := rand.Int()
		filename := fmt.Sprintf("data/output/%d.txt", runID)
		f, err := os.Create(filename)
		check(err)

		w := bufio.NewWriter(f)
		for _, num := range buff {
			fmt.Fprintf(w, "%d ", num)
		}
		fmt.Fprint(w, "\n")

		err = w.Flush()
		check(err)

		f.Close()
	}

	complete <- true
}

// Split input for external sorting
func inputSplit(file string) {
	// Concurrency variables
	linesChannel := make(chan string)
	writeChannel := make(chan []int)
	completeLines := make(chan bool)
	completeWrite := make(chan bool)
	sortConcurrency := getEnv("SORT_CONCURRENCY", "100")
	writeConcurrency := getEnv("WRITE_CONCURRENCY", "100")

	go func() {
		f, err := os.Open(file)
		check(err)

		scanner := bufio.NewScanner(f)

		for scanner.Scan() {
			curr := scanner.Text()
			linesChannel <- curr
		}

		f.Close()
		close(linesChannel)
	}()

	// Wait for goroutines to finish
	for i := 0; i < sortConcurrency; i++ {
		go sortRun(linesChannel, writeChannel, completeLines)
	}
	for i := 0; i < writeConcurrency; i++ {
		go writeRun(writeChannel, completeWrite)
	}
	for i := 0; i < sortConcurrency; i++ {
		<-completeLines
	}
	close(writeChannel)
	for i := 0; i < writeConcurrency; i++ {
		<-completeWrite
	}
}

func main() {
	// Parse CLI arguments
	file := os.Args[1]
	n, err := strconv.Atoi(os.Args[2])
	check(err)

	inputSplit(file)
	topN(n)
}
